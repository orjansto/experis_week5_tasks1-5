﻿**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis Week5 Tasks 1..5
## Task 1
- [x] Displaying fibonacci numbers 
- [x] Made JavaScript, not html

## Task 2
- [x] Displaying FizzBuzz
- [x] Made JavaScript, not html

## Task 3
- [x] Displaying analog clock using moment

## Task 4
- [x] Made simple calculator

## Task 5
- [x] Rock, Paper, Scissor- game
- [x] Animations for game part
