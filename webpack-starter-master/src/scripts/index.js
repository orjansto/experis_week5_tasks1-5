import '../styles/index.scss';
import * as moment from 'moment';
import { setTimeout } from 'core-js';

initLocalClocks();
//Clock function 
function initLocalClocks() {
    // Get the local time using Moment
    
    var seconds = moment().format('ss');
    var minutes = moment().format('mm');
    var hours = moment().format('h');
    
    // Create an object with each hand and it's angle in degrees
    var hands = [
      {
        hand: 'hours',
        angle: (hours * 30) + (minutes / 2)
      },
      {
        hand: 'minutes',
        angle: (minutes * 6)
      },
      {
        hand: 'seconds',
        angle: (seconds * 6)
      }
    ];
    // Loop through each of these hands to set their angle
    for (var j = 0; j < hands.length; j++) {
      var elements = document.querySelectorAll('.' + hands[j].hand);
      for (var k = 0; k < elements.length; k++) {
          elements[k].style.webkitTransform = 'rotateZ('+ hands[j].angle +'deg)';
          elements[k].style.transform = 'rotateZ('+ hands[j].angle +'deg)';
          // If this is a minute hand, note the seconds position (to calculate minute position later)
          if (hands[j].hand === 'minutes') {
            elements[k].parentNode.setAttribute('data-second-angle', hands[j + 1].angle);
          }
      }
    }
}




//Rock Paper Scissor!
const actionResult_div = document.querySelector(".action-result");
const rock_img = document.getElementById("rock");
const paper_img = document.getElementById("paper");
const scissor_img = document.getElementById("scissor");


function getComputerChoice(){
    const choises = ['rock', 'paper', 'scissor'];
    const randomNumber = Math.floor(Math.random()*3);
    return choises[randomNumber];
}
function win(user, computer){
    actionResult_div.innerHTML = `${user} beats ${computer} WIN!`;
}
function loose(user, computer){
    actionResult_div.innerHTML = `${user} loose to ${computer} LOSS!`;
}
function draw(user, computer){
    actionResult_div.innerHTML = `${user} draws with ${computer} DRAW!`;
}


function game(userChoice){
    const computerChoise = getComputerChoice();
    gameAnimations(userChoice,computerChoise);
}

function determinWinner(userChoice,computerChoise){
    switch (userChoice + computerChoise){
        case "rockscissor":
        case "paperrock":
        case "scissorpaper":
            win(userChoice,computerChoise);
            break;
        case "scissorrock":
        case "rockpaper":
        case "paperscissor":
            loose(userChoice,computerChoise);
            break;
        case "rockrock":
        case "paperpaper":
        case "scissorscissor":
            draw(userChoice,computerChoise);
            break;
    } 

}

function gameMain(){
    rock_img.addEventListener('click', function(){
        game("rock");       
    });

    paper_img.addEventListener('click', function(){
        game("paper");
    });

    scissor_img.addEventListener('click', function(){
        game("scissor");
    });
}
gameMain();


//Animations
function gameAnimations(myMove, eneMove){
  var myElem = document.getElementById("myAnimation");
  var myElemImg = myElem.getElementsByTagName("img")[0];
  var choiceButtons = document.getElementById("choiceButtons");

  var eneElem = document.getElementById("eneAnimation");
  var eneElemImg = eneElem.getElementsByTagName("img")[0]; 

  var pos = 0;
  var times = 0;
  var negpos = 0;
  var id = setInterval(frame, 8);
  function frame() {
    choiceButtons.style.pointerEvents='none';
    if (pos == 90) {
        times++;
        if(times == 3){
            clearInterval(id);
            myElemImg.src="/src/img/"+myMove+".png";
            eneElemImg.src="/src/img/ene"+eneMove+".png";
            determinWinner(myMove,eneMove);
            
            setTimeout(function() {
                myElemImg.src ="/src/img/rock.png";
                eneElemImg.src ="/src/img/enerock.png";
                choiceButtons.style.pointerEvents='auto';
            }, 2000);
            
        }
        pos=0;  
    } 
    else if(pos <= 45){
        pos++;
        myElem.style.transform ='rotate('+ pos + 'deg)'; 
        eneElem.style.transform ='rotate('+ (-pos) + 'deg)';    
    }
    else {
        pos++;
        negpos = 90-pos;
        myElem.style.transform ='rotate('+ negpos + 'deg)';
        eneElem.style.transform ='rotate('+ (-negpos) + 'deg)';
    }
  }
}
global.myMove = gameAnimations;